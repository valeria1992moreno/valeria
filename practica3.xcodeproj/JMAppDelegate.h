//
//  JMAppDelegate.h
//  practica3
//
//  Created by Walos on 04/03/14.
//  Copyright (c) 2014 jesus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
