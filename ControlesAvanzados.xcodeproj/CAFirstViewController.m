//
//  CAFirstViewController.m
//  ControlesAvanzados
//
//  Created by Walos on 05/03/14.
//  Copyright (c) 2014 jesus. All rights reserved.
//

#import "CAFirstViewController.h"

@interface CAFirstViewController ()

@end

@implementation CAFirstViewController
NSArray *unidades;
int tipoDeUnidad=0;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    unidades=[NSArray arrayWithObjects:[NSArray arrayWithObjects:@"longitud",@"centimetros",@"metros",@"kilometros",@"pie", nil],[NSArray arrayWithObjects:@"Area",@"hectareas",@"metros cuadrados", nil],[NSArray arrayWithObjects:@"Volumen",@"litros",@"metros cubicos", nil], nil];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    unidades=nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{

    return 2;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    if(component==0)
        return [unidades count];
    return [[unidades objectAtIndex:tipoDeUnidad]count]-1;
}
-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{

    if(component==0)
        if(tipoDeUnidad !=row){
            tipoDeUnidad=row;
    [pickerView reloadComponent:1];
}
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    if(component==0)
        return [[unidades objectAtIndex:row]objectAtIndex:0];
    return [[unidades objectAtIndex:tipoDeUnidad]objectAtIndex:row+1];
}


@end
