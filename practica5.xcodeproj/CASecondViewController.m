//
//  CASecondViewController.m
//  practica5
//
//  Created by Walos on 05/03/14.
//  Copyright (c) 2014 jesus. All rights reserved.
//

#import "CASecondViewController.h"

@interface CASecondViewController ()

@end

@implementation CASecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
